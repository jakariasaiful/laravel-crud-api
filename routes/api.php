<?php

use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\SectorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::controller(UserController::class)->group(function () {
    Route::post('/user-login', 'login');
    // Route::middleware('auth:sanctum')->post('/user-logout', 'logout');
   
});




Route::get('/get-all-sectors', [SectorController::class, 'getAllSectors']);
Route::post('/save-employee', [EmployeeController::class, 'store']);
Route::get('/get-all-employee', [EmployeeController::class, 'index']);
Route::post('/update-employee', [EmployeeController::class, 'update']);
Route::get('/delete-employee/{id}', [EmployeeController::class, 'destroy']);


// if logedin



