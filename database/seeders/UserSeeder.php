<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->user_type = 3;
        $user->first_name = "Admin";
        $user->last_name = "User";
        $user->user_name =  "admin";
        $user->email = "admin@email.com";
        $user->password = Hash::make("admin123");
        $user->email_verification_id = rand(888888, 999999);
        $user->save();
    }
}