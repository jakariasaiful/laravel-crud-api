<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    


    // User Login
    public function login(Request $request)
    {
        
        try {
            // check validation
            $validator = Validator::make($request->all(), [
                'email' => ['required', 'email:rfc,dns'],
                'password' => ['required'],
            ]);

            if ($validator->fails()) {
                return $this->sendResponse(false, null, $validator->errors(), 401);
            }
            $remember = false;
            if($request->has('remember_me') && $request->remember_me){
                $remember = true;
            }

            // check authentication
            if(Auth::attempt($request->only('email', 'password'), $remember)){
                $user = User::where('email', $request->email)->first();
                $payload = [
                    'token' => $user->createToken('API TOKEN')->plainTextToken,
                    'email' => $user->email,
                    'is_email_verified' => true,
                ];
                if($user->is_block){
                    return $this->sendResponse(false, null, 'Your profile has been blocked by admin!', 401);
                }
                if(!$user->email_verified_at){
                    $payload = [
                        'token' => $user->createToken('API TOKEN')->plainTextToken,
                        'email' => $user->email,
                        'is_email_verified' => false,
                    ];
                    return $this->sendResponse(false, $payload, 'Email is not verified! please verify your email.', 200);
                }

               
                
                return $this->sendResponse(true, $payload, 'User Login Successfully!', 200);
                
            }
            return $this->sendResponse(false, null, 'Email and password does not match!', 401);

            
        
        } catch (\Throwable $th) {
            return $this->sendResponse(false, null, $th->getMessage(), 500);
        }

    }

   

}