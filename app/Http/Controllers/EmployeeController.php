<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = Employee::orderBy('created_at', 'DESC')->with([
                'sectorInfo', 
            ])->get();
            return $this->sendResponse(true, $data, 'Employee list get successfully!', 200);
         
         } catch (\Throwable $th) {
             return $this->sendResponse(false, null, $th->getMessage(), 500);
         } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => ['required'],
                'sector' => ['required'],
            ]);

            
            if ($validator->fails()) {
                return $this->sendResponse(false, null, $validator->errors(), 401);
            }

            // if Upload document validation
            if ($request->has('documents')){
                $validator = Validator::make($request->all(), [
                    'documents.*' => [
                        'required', 
                        'mimes:pdf,xlx,csv,doc,docx',
                        'max:2048'
                    ]
                ]);

                if ($validator->fails()) {
                    return $this->sendResponse(false, null, $validator->errors(), 401);
                }
            }

            $employee = new Employee();
            $employee->name = $request->name;
            $employee->sector_id = $request->sector;
            $employee->is_term_accept = true;
            $employee->save();

            

            return $this->sendResponse(true, null, 'Employee created successfully!', 200);
        
        } catch (\Throwable $th) {
            return $this->sendResponse(false, null, $th->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        try {
            Employee::whereId($request->id)->update([
                'name' => $request->name,
                'sector_id' => $request->sector_id,
            ]);
            return $this->sendResponse(true, null, 'Employee updated successfully!', 200);
        } catch (\Throwable $th) {
            return $this->sendResponse(false, null, $th->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Employee::where([
                ['id', '=', $id]
            ])->delete();
            return $this->sendResponse(true, null, 'Employee deleted successfully!', 200);
        } catch (\Throwable $th) {
            return $this->sendResponse(false, null, $th->getMessage(), 500);
        }
    }
}
