<?php

namespace App\Http\Controllers;

use App\Models\Sector;
use Illuminate\Http\Request;

class SectorController extends Controller
{
    public function getAllSectors(Sector $sector){
        try {
            $sectors = $sector->all();

            return $this->sendResponse(true, $sectors, 'Sector get successfully!', 200);
        
        } catch (\Throwable $th) {
            return $this->sendResponse(false, null, $th->getMessage(), 500);
        }
    }
}
