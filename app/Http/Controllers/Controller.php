<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
   
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function sendResponse($success = false, $payload = null, $message = null, $status_code = 500){
        $responseModel =  [
            'success'=> $success,
            'payload'=> [
                "data"=> $payload
            ],
            'message'=> $message,
            'status_code' => $status_code,
        ];

        return response()->json($responseModel, $status_code);
        
    }

    public function getDynamicUploadPath(){
        $year = date("Y");   
        $month = date("m"); 
        return "uploads/".$year."/".$month;
    }
}
