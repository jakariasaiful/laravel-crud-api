<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'sector_id'
    ];

    public function sectorInfo(){
        return $this->hasOne(Sector::class, 'id', 'sector_id');
    }
}
